import React, { Component } from "react";
import ButtonLabeledIcon from "./button";
import ButtonToggleTautograma from "./button_tauto";
import DisplayName from "./display";
import { Grid, Container } from "semantic-ui-react";
import { API_Request, API_URL } from "./api";

class Random extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      tautograma: false,
      btn_tautograma: "Tautograma",
      btn_random: "Random",
      icon_random: "random"
    };
    this.onClickRandom = this.onClickRandom.bind(this);
    this.onClickTautograma = this.onClickTautograma.bind(this);
  }

  onClickRandom = () => {
    fetch(API_URL, API_Request({ tautograma: this.state.tautograma }))
      .then(function(response) {
        return response.text();
      })
      .then(function(data) {
        return JSON.parse(data).result;
      })
      .then(data => this.setState({ name: data }))
      .catch(function(error) {
        console.error(error);
      });
  };
  onClickTautograma = () => {
    const prev = { ...this.state };
    this.setState({ tautograma: !prev.tautograma });
  };
  render() {
    return (
      <Container>
        <Grid centered>
          <Grid.Row centered>
            <Grid.Column>
              <DisplayName name={this.state.name} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <ButtonLabeledIcon
              name={this.state.btn_random}
              icon={this.state.icon_random}
              onClick={this.onClickRandom}
            />
            <ButtonToggleTautograma
              state={this.state.tautograma}
              btn={this.state.btn_tautograma}
              onClick={this.onClickTautograma}
            />
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default Random;
