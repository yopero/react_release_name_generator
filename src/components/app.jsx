import React, { Component, Fragment } from 'react';
import MainMenu from './menu'
import MyHeader from './header'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
            <Fragment>
                <MyHeader/>
                <MainMenu/>
            </Fragment>
        );
    }
}
 
export default App;
