function API_Request({
  tautograma = false,
  letra = null,
  grupo = "random"
} = {}) {
  const payload = JSON.stringify({
    args: {
      tautograma: tautograma,
      letra: letra,
      grupo: grupo
    }
  });
  const query = {
    method: "POST",
    body: payload,
    headers: {
      "Content-Type": "text/plain",
      Accept: "application/json"
    }
  };

  return query;
}

const API_URL =
  "https://n9ngd4ga75.execute-api.eu-west-1.amazonaws.com/prod/release_name_generator";
export {
  API_Request,
  API_URL
}
// export default API_Request;
