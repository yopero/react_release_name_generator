import React from 'react'
import { Button, Icon } from 'semantic-ui-react'

function ButtonToggleTautograma(props) {
    return (
      <Button
        toggle
        active={props.state}
        onClick={event => props.onClick(event)}
        size="mini"
      >
        <Icon name={props.state ? "check" : "cancel"} />
        {props.btn}
      </Button>
    );
}

export default ButtonToggleTautograma