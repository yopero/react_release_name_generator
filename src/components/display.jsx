import React from 'react'
import { Header } from 'semantic-ui-react'

const DisplayName = (props) => <Header as="h1" block color="teal" textAlign="center">{props.name}</Header>;

export default DisplayName