import React from "react";
import { Button, Icon } from "semantic-ui-react";

function ButtonLabeledIcon(props) {
  return (
    <Button onClick={props.onClick} icon labelPosition="left">
      <Icon name={props.icon} />
      {props.name}
    </Button>
  );
}

export default ButtonLabeledIcon;
