import React, { Component } from "react";
import { Button, Container, Grid } from "semantic-ui-react";
import DisplayName from "./display";
import {API_Request, API_URL} from "./api";
const letters = "abcdefghijklmnopqrstuvwxyz".split('');

class ButtonsAZ extends Component {
  constructor(props) {
    super(props);
    this.state = { name: this.props.name};
    this.manageClick = this.manageClick.bind(this);
  }
  manageClick = e => {
    const letra = e.target.value;
    fetch(API_URL, API_Request({tautograma: true, letra: letra})
    )
      .then(function(response) {
        return response.text();
      })
      .then(function(data) {
        return JSON.parse(data).result;
      })
      .then(data => this.setState({ name: data }))
      .catch(function(error) {
        console.error(error);
      });
  };
  render() {
    return (
      <Container>
        <Grid>
          <Grid.Row centered>
            <Grid.Column>
              <DisplayName name={this.state.name} />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row stretched centered>
            {letters.map(letter => (
              <Button
                style={{
                  boxSising: "border-box",
                  margin: "3px",
                  borderStyle: "outset",
                  borderWidth: "1.3px"
                }}
                key={letter}
                value={letter}
                onClick={this.manageClick}
              >
                {letter.toUpperCase()}
              </Button>
            ))}
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

export default ButtonsAZ;
