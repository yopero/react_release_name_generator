import React, {Component} from "react";
import { Tab, Container } from "semantic-ui-react";
import Random from "./random";
import ButtonsAZ from "./alphabet";
import ButtonGroups from "./group";

const default_name = "**** ****"

const panes = [
  {
    menuItem: "Random Name",
    render: () => (
      <Tab.Pane attached={false}>
        <Random name={default_name} />
      </Tab.Pane>
    )
  },
  {
    menuItem: "Name starting with letter",
    render: () => (
      <Tab.Pane attached={false}>
        <Container>
          <ButtonsAZ name={default_name} />
        </Container>
      </Tab.Pane>
    )
  },
  {
    menuItem: "Name from group",
    render: () => (
      <Tab.Pane attached={false}>
        <Container>
          <ButtonGroups name={default_name} />
        </Container>
      </Tab.Pane>
    )
  }
];

class MainMenu extends Component {
  render() {
    return (
      <Container>
        <Tab
          menu={{ color: "olive", secondary: true, pointing: true }}
          panes={panes}
        />
      </Container>
    );
  }
}

export default MainMenu;

