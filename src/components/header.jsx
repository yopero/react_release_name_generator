import React from "react";
import { Message, Header, Icon } from "semantic-ui-react";

const icon = (
  <Icon name="settings" color="teal"/>
);
const extra = (
  <Header color="teal" relaxed="true" as="h1">
    
    <Header.Content>
      Release Name Generator
      <Header.Subheader>
        Humans tend to prefer names rather than numbers, a set of codenames are
        used by developers and testers during the buildup to a release.
      </Header.Subheader>
    </Header.Content>
  </Header>
)
const MyHeader = () => (
  <Message
    // info
    size="large"
    icon={icon}
    header={extra}
  />
);

export default MyHeader;
